import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseListener;
import java.util.concurrent.ThreadLocalRandom;
import java.util.List;

public abstract class Toolbox {
    public static void setAll(List list, int value) {
        for (int i=0;i<list.size();i++)
            list.set(i, value);
    }
    public static void setJComboBoxReadOnly(JComboBox jcb) {
        JTextField jtf = (JTextField) jcb.getEditor().getEditorComponent();
        jtf.setEditable(false);

        MouseListener[] mls = jcb.getMouseListeners();
        for (MouseListener listener : mls)
            jcb.removeMouseListener(listener);

        Component[] comps = jcb.getComponents();
        for (Component c : comps) {
            if (c instanceof AbstractButton) {
                c.setEnabled(false);

                MouseListener[] mls2 = c.getMouseListeners();
                for (MouseListener listener : mls2)
                    c.removeMouseListener(listener);
            }
        }
    }
}
